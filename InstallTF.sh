#!/bin/bash

cd ~
sudo yum install -y zip unzip

wget https://releases.hashicorp.com/terraform/0.9.8/terraform_0.9.8_linux_amd64.zip


unzip terraform_0.9.8_linux_amd64.zip.

sudo mv terraform /usr/local/bin/
terraform --version.