output "hostname" {
  value = "${aws_instance.web.*.private_dns}"
}

output "addresses" {
  value = ["${aws_instance.web.*.private_ip}"]
  sensitive   = true
}

output "winhostname" {
  value = "${aws_instance.win.private_dns}"
}

output "winaddresses" {
  value = ["${aws_instance.win.private_ip}"]
  sensitive   = true
}


