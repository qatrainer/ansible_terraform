output "hostname" {
  value = "${aws_instance.*.*.hostname}"
}

output "addresses" {
  value = ["${aws_instance.*.*.private_ip}"]
}

